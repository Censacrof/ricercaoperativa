clear;
format rational;

% inputs
B = [3 4];


A = [
    -6 -10;
    7 2;
    4 -3;
    -2 -4;
    -2 2;
    -2 -1;
];
b = [17 28 16 3 3 3]';
c = [3 1]'; % ATTENZIONE: questo simplesso massimizza
% fine inputs

basi_esplorate = [];
while true
    basi_esplorate = [basi_esplorate; B;];
    B = passoSimplessoPrimale(A, b, c, B);
    if isempty(B)
        break;
    end
end

basi_esplorate

function B_new = passoSimplessoPrimale(A, b, c, B)
    fprintf("##############################################");

    B_new = [];
    
    B

    N = 1:1:size(A, 1);
    N(B) = []
    
    A_B = A(B, :)
    A_BI = inv(A_B)
    b_B = b(B)
    
    % soluzione di base primale
    x_ = A_BI * b_B
    
    % soluzione di base duale
    y_ = zeros(size(A, 1), 1);
    y_(B) = (c' * A_BI)'
    
    % se anche y_ è ammissibile
    if all(y_(B) >= 0)
        fprintf("Siamo all'ottimo")
        return
    end
    
    % indice uscente
    temp = 1:1:size(A, 1);
    h = min(temp(y_ < 0))
    
    % W e W_h
    W = -A_BI
    temp = 1:1:length(B);
    index = temp(B == h);
    W_h = W(:, index)
    
    % calcoliamo i rapporti
    denominatori = [];
    r = zeros(length(N), 1) + Inf;
    for index = 1:1:length(N)
        i = N(index);
        
        den = A(i, :) * W_h;
        denominatori = [denominatori; den;];
        
        if den > 0        
            r(index) = (b(i) - A(i, :) * x_) / den;
        end
    end
    denominatori
    fprintf("ATTENZIONE: clicca sul risultato per espanderlo (1 / 0 = Inf ingoralo)");
    r
    
    % se tutti i denominatori sono <= 0
    if all(denominatori <= 0)
        fprintf("P ha valore ottimo +inf e D è vuoto")
        return
    end
    
    % theta
    theta = min(r(denominatori > 0))
    
    % indice entrante
    k = min(N(denominatori > 0 & r == theta))
    
    % calcolo la nuova base
    B_new = B;
    B_new(find(B_new == h)) = [];
    B_new = sort([B_new k])

    return
end
