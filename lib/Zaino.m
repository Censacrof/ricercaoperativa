clear();
clc();
format rat;

vol_max = 200;

% usa vettori riga
val = [300 100 250 200 400 120 100 100 400];
vol = [50 40 30 25 40 50 60 40 30];


% fine inputs

% ricaviamo la prima vi e vs
rap = val ./ vol
[~, ordine] = sort(rap, 'desc')


[x_vs, vs] = ZainoVs(val, vol, ordine, vol_max, [-1 -1 -1 -1 -1 -1 -1 -1 -1])
x_vi = floor(x_vs);

vi = floor(x_vi * val')

% le altre ricavale usando la funzione ZainoVs insieme al brach and bound


