format rational;
clear;

clc;
% flusso minimo capacitato

% lista degli archi (i, j)
A = sortrows([ % i j cost_ij cap_ij
    1 2 7 10;
    1 3 4 8;
    1 4 8 7;
    
    2 4 4 9;    
    
    3 5 10 9;    
    
    4 3 5 10;
    4 6 9 9;
    
    5 4 4 12;
    5 7 5 5;
    
    6 5 3 10;
    6 7 7 6;
]);

b = [-2 -3 -6 4 3 4 -0]';

T = sortrows([
    1 3;
    2 4;
    3 5;
    4 6;
    5 4;
    6 7;
]);

U = sortrows([
    3 5;
]);

% fine inputs
cost = A(:, 3);
cap  = A(:, 4);
A(:, 3) = [];
A(:, 3) = []; % non è un errore

A
cost
cap

L = sortrows(setdiff(setdiff(A, T, 'rows'), U, 'rows'));

T_ind = []; % indici di T in A
L_ind = []; % indici di L in A
U_ind = []; % indici di U in A
for i = 1:1:size(A, 1)
    for j = 1:1:size(T, 1)
        if A(i, :) == T(j, :)
            T_ind = [T_ind i];
        end
    end
    
    for j = 1:1:size(L, 1)
        if A(i, :) == L(j, :)
            L_ind = [L_ind i];
        end
    end
    
    for j = 1:1:size(U, 1)
        if A(i, :) == U(j, :)
            U_ind = [U_ind i];
        end
    end
end

% matrice di incidenza
E = getIncidence(A)
Adj = incidenceToAdjacence(E)

% flusso di base
E_T = E(:, T_ind); % E_T senza la prima riga
E_T(1, :) = [];
E_TI = inv(E_T);

temp = b - E(:, U_ind) * cap(U_ind);
temp(1, :) = [];
x_T = E_TI * temp;

x_ = zeros(size(A, 1), 1);
x_(T_ind) = x_T;
fprintf("Flusso di base:\n");
x_(U_ind) = cap(U_ind) % flusso di base

% potenziale di base
fprintf("Potenziale di base:\n");
p = [0; (cost(T_ind)' * E_TI)';]


% condizioni di bellman
costi_ridotti = [];
for k = 1:1:size(A, 1)
    i = A(k, 1);
    j = A(k, 2);
    
    costi_ridotti = [
        costi_ridotti;
        cost(k) + p(i) - p(j);
    ];
end
costi_ridotti

% se le condizioni di bellman sono soddisfatte
if all(costi_ridotti(L_ind) >= 0) && all(costi_ridotti(U_ind) <= 0)
    fprintf("Il flusso é ottimo");
    return;
end

% altrimenti calcolo l'arco entrante
entrante_ind = []; % indice in A del primo arco che viola le condizioni di bellman
entrante_parte_di_L = true;
for k = 1:1:size(A, 1)    
    if any(L_ind == k)
        if costi_ridotti(k) < 0
            entrante_ind = k;
            break;
        end
    end
    
    if any(U_ind == k)
        if costi_ridotti(k) > 0
            entrante_ind = k;
            entrante_parte_di_L = false;
            break;
        end
    end
end
entrante = A(entrante_ind, :)

% trovo il ciclo che 'entrante' forma con gli archi di T
start = [];
target = [];
if entrante_parte_di_L % => ciclo concorde
    start = entrante(1);
    target = entrante(2);
else % => fa parte di U => ciclo discorde
    start = entrante(2);
    target = entrante(1);
end

% preparo una matrice di adiacenza da poter usare con Dijkstra
E_T_ = E(:, T_ind); % E_T_ CON la prima riga
Adj_T = zeros(size(E_T_, 1));
for j = 1:1:size(E_T_, 2)
    a = [];
    b = [];
    for i = 1:1:size(E_T_, 1)
        if E_T_(i, j) == -1
            a = i;
        end
        
        if E_T_(i, j) == 1
            b = i;
        end
    end
    Adj_T(a, b) = 1;
    Adj_T(b, a) = 1;
end

[~, prev] = Dijkstra(Adj_T, start, false);
i = target;
cycle = entrante;
concordi = [];
discordi = [];
if entrante_parte_di_L
    concordi = [concordi; entrante;];
else
    discordi = [discordi; entrante;];
end

while i ~= start
    next = prev(i);
    
    arco = [];
    if Adj(i, next) == 1 % l'arco é concorde
        arco = [i next];
        concordi = [concordi; arco;];
    else
        arco = [next i]; % l'arco é discorde
        discordi = [discordi; arco;];
    end
    
    cycle = [
        cycle;
        arco;
    ];

    i = next;
end
cycle
concordi = sortrows(concordi) % ordine lessicografico
discordi = sortrows(discordi)

% calcolo i theta
con_ind = []; % indici di concordi in A
dis_ind = []; % indici di discordi in A
for i = 1:1:size(A, 1)
    for j = 1:1:size(concordi, 1)
        if A(i, :) == concordi(j, :)
            con_ind = [con_ind i];
        end
    end
    
    for j = 1:1:size(discordi, 1)
        if A(i, :) == discordi(j, :)
            dis_ind = [dis_ind i];
        end
    end
end


temp = cap - x_;
theta_p = min(temp(con_ind));
if isempty(theta_p)
    theta_p = inf;
end
theta_p

theta_m = min(x_(dis_ind))
if isempty(theta_m)
    theta_m = inf;
end

theta = min(theta_p, theta_m)


% arco uscente
temp = cap - x_;
temp_con = [];
for i = 1:1:length(con_ind)
    if temp(con_ind(i) == theta)
        temp_con = [
            temp_con;
            A(con_ind(i), :);
        ];
    end
end


temp_dis = [];
for i = 1:1:length(dis_ind)
    if x_(dis_ind(i)) == theta
        temp_dis = [
            temp_dis;
            A(dis_ind(i), :);
        ];
    end
end

temp = sortrows([
    temp_con;
    temp_dis;
]);

uscente = temp(1, :)
uscente_concorde = false;
if ~isempty(concordi) && any(ismember(uscente, concordi, 'rows'))
    uscente_concorde = true;
end


% aggiorno la tripartizione TODO
entrante_parte_di_L
uscente_concorde
T_new = T;
L_new = L;
U_new = U;
% if entrante_parte_di_L
%     if uscente_concorde
%         if entrante == uscente
%             [~, ind] = ismember(entrante, L_new, 'rows');
%             L_new(ind) = [];
%             
%             U_new = sortrows([U_new entrante]);
%         else
%             [~, ind] = 
%         end
%     end
% else % entrante parte di U
%     
% end


function E = getIncidence(A)
    s = A(:, 1);
    d = A(:, 2);

    n = max([max(s), max(d)]);
    a = length(s);
    
    E = zeros(n, a);
    for i = 1:1:length(s)
        E(s(i), i) = -1;
        E(d(i), i) = 1;
    end
end

function Adj = incidenceToAdjacence(E)
    Adj = zeros(size(E, 1));
            
    for j = 1:1:size(E, 2)
        a = [];
        b = [];
        for i = 1:1:size(E, 1)
            if E(i, j) == -1
                a = i;
            end
            
            if E(i, j) == 1
                b = i;
            end
        end
        Adj(a, b) = 1;
    end
end