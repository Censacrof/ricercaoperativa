function [dist, prev] = Dijkstra(Adj, start, isVerbose)
    switch nargin
        case 0
            assert(false, "La matrice di adiacenza non é un parametro opzionale.");
        
        case 1
            assert(false, "Il nodo di partenza non é un parametro opzionale.");            
    end    
    
    assert(size(Adj, 1) == size(Adj, 2), "La matrice di adiacenza deve essere quadrata.");
    
    n = size(Adj, 1); % numero di nodi
    
    assert(start <= n && start >= 0, "Il nodo di partenza non fa parte del grafo.");
    
    if ~exist('isVerbose', 'var')
        isVerbose = true;
    end
    
    if isVerbose
        fprintf("----------------------------------------------\n");
    end
    
    dist = zeros(1, n) + inf;
    dist(start) = 0;
    
    prev = zeros(1, n) - 1;
    prev(start) = 0;
    
    U = 1:1:n;
    while ~isempty(U)
        m = inf;
        i_min = [];
        for i = 1:1:length(U)
            if dist(U(i)) < m
                m = dist(U(i));
                i_min = i;
            end
        end
        
        if (m == inf)
            break;
        end
        
        u = U(i_min);
        U(i_min) = [];
        
        if isVerbose
            u
        end
        
        for v = 1:1:n
            if Adj(u, v) <= 0
                continue;
            end
            
            if u == v
                continue;
            end
            
            if dist(v) > dist(u) + Adj(u, v)
                prev(v) = u;
                dist(v) = dist(u) + Adj(u, v);
            end
        end
        
        if isVerbose
            prev
            dist
            fprintf("----------------------------------------------\n");
        end
    end
end
