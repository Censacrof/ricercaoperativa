function eq = RettaPassante2Punti(a, b)
    syms x;
    syms y;
    
    eq = simplify((x - a(1)) / (b(1) - a(1)) == (y - a(2)) / (b(2) - a(2)));
end

