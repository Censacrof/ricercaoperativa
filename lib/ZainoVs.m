function [x_vs, vs] = zainoVs(val, vol, ordine, vol_max, default_xvs)
    assert(nargin == 5, "Passa tutti gli argomenti");
    assert(length(val) == length(vol) && length(val) == length(ordine) && length(val) == length(default_xvs), "val, vol, ordine e default_xvs devono essere vettori della stessa lunghezza\n");
    assert(size(val, 1) == 1 && size(vol, 1) == 1 && size(ordine, 1) == 1 && size(default_xvs, 1) == 1, "val, vol, ordine e default_xvs devono essere vettori riga\n");

    x_vs = zeros(1, length(ordine));
    x_vs(default_xvs == 1) = 1;

    vol_max = vol_max - x_vs * vol';
    
    for i = 1:1:length(ordine)
        if default_xvs(ordine(i)) ~= -1
            continue;
        end
        
        v = vol(ordine(i));
        r = vol_max / v;
        
        if r >= 1
            vol_max = vol_max - v;
            x_vs(ordine(i)) = 1;
        else
            vol_max = vol_max - r;
            x_vs(ordine(i)) = r;
            break;
        end
    end
    
    vs = floor(val * x_vs');
end

