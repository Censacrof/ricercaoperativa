clear();
clc();
format rat;

syms f(x1, x2);
f(x1, x2) = 2*x1*x2 + x1 + 4*x2;

A = [
    -2 3;
    6 -1;
    0 -1;
    -1 0;
];

b = [3 -1 5 3]';
x = [-3 -11/3]';

nIterazioni = 1;
isMin = true;

% fine inputs
for i = 1:1:nIterazioni
    xnew = passoGradienteProiettato(f, x, A, b, isMin);
    
    if xnew == x
        break;
    end
    
    x = xnew;
end


function x_new = passoGradienteProiettato(f, x, A, b, isMin)
    x_new = x;
    fprintf("-------------------------------------------\n");    

    if nargin ~= 5
        assert(false, "Metti tutti gli argomenti");
    end
    
    minOrMax = 1;
    if isMin
        minOrMax = -1;
    end
    
    % trovo il gradiente di f
    grad_f = gradient(f)
    
    % trovo l'insieme dei vincoli attivi e M
    J = find(A * x == b)
    M = A(J, :)
    
    % trovo H
    H = [];
    if isempty(J)
        H = eye(size(A, 2));
    else
        H = eye(size(A, 2)) - M'*inv(M*M')*M;
    end
    H
    
    
    % trovo la direzione d
    d = minOrMax * H * grad_f(x(1), x(2))
    if norm(d) == 0
        fprintf("x è un punto stazionario\n");
        x
        return;
    end
    
    
    % trovo il passo massimo
    A_lp = double(A * d);
    b_lp = double(b - A * x);
    bounds_lp = b_lp(A_lp ~= 0) ./ A_lp(A_lp ~= 0);
    bounds_lp = bounds_lp(bounds_lp >= 0);
    t_max = min(bounds_lp)
    %t_max = linprog(-1, A_lp, b_lp, [], [], 0, []);
    
    % trovo il passo
    syms phi(t);
    phi(t) = simplify(f(x(1) + t * d(1), x(2) + t * d(2)))
    
    phi_d1 = diff(phi, t) % derivata di phi (devo cercare il minimo / massimo di phi)
    
    phi_staz = [solve(phi_d1(t) == 0, t) 0 t_max]   % trovo tutti i punti in cui la derivata si annulla e aggiungo gli estremi 0  t
    phi_staz(phi_staz < 0) = [] % e scarto quelli che non stanno nell'intervallo [0, t_max]
    phi_staz(phi_staz > t_max) = []
    
    values = phi(phi_staz) % calcolo i valori di phi dove la sua derivata si annulla
    
    t = []; % faccio l'argmin
    if minOrMax == -1 % problema di minimo
        [~, index] = min(values);
        t = phi_staz(index);
    else % problema di massimo
        [~, index] = max(values);
        t = phi_staz(index);
    end
    t
    
    if t == 0
        fprintf("Devi fare l'analisi locale con lkkt\n");
        return;
    end
    
    x_new = [x(1) + t * d(1); x(2) + t * d(2);]
end
