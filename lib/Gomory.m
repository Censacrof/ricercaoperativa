% ATTENZIONE: ricordarsi di portarsi nella forma giusta (pag. 248)
clear;
clc();
format rational;

% SOLUZIONE RC
c = -[8 11]'; % ATTENZIONE: questo simplesso minimizza
A_rc = [      % omettere i lower bound
    30 20;
    10 20;
    20 0;
    0 30;
];
b = [120 85 62 105]';

% fine inputs
DECIMAL_PLACES = 6;
lb = zeros(1, size(A_rc, 2));
x_rc = linprog(c, A_rc, b, [], [], lb, []);
x_rc = round(x_rc, DECIMAL_PLACES);

% scarto da A_rc e b le righe non di base_rc
ind = (A_rc * x_rc ~= b);
A_rc(ind, :) = [];
b(ind) = [];

% MATRICE A
A = [];
for i = 1:1:size(A_rc, 1) % per ogni riga di A_rc
    scarto = zeros(1, size(A_rc, 1));
    scarto(i) = 1;
    A(i, :) = [A_rc(i, :) scarto];
end

% TROVO x_ COMPRESO DI VARIABILI DI SCARTO RISOLVENDO IL SISTEMA
A__lower = eye(size(A_rc, 2), size(A, 2));
A__ = [
    A;
    A__lower;
];
b__ = [b; x_rc;];

x_ = inv(A__) * b__;
x_ = round(x_, DECIMAL_PLACES)

% TROVO B e N
B = [];
N = [];
for i = 1:1:length(x_)
    if x_(i) ~= 0
        B = [B i];
    else
        N = [N i];
    end
end
B
N

% TROVIAMO b_tilde
b_tilde = x_(B)

% TROVIAMO A_tilde
A_B = A(:, B)
A_BI = round(inv(A_B), DECIMAL_PLACES)

A_N = A(:, N)

A_tilde = A_BI * A_N

% CALCOLIAMO IL TAGLIO
for i = 1:1:length(B)   
    
    str = sprintf("per r = %d: ", B(i));
    str2 = " = ";
    if fract(b_tilde(i)) ~= 0
        for k = 1:1:length(N)
            if k > 1
                str = str + " + ";
                str2 = str2 + " + ";
            end
            
            str = str + sprintf("{%s} x_%d", strtrim(rats(A_tilde(i, k))), N(k));
            str2 = str2 + sprintf("%s x_%d", strtrim(rats(fract(A_tilde(i, k)))), N(k));
        end
        
        str2 = str2 + sprintf(" >= {%s} = %s", strtrim(rats(b_tilde(i))), strtrim(rats(fract(b_tilde(i)))));
    else
         
    end
    
    str = str + str2
end

function f = fract(x)
    f = x - floor(x);
end

