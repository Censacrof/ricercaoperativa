function [x_vi, vi] = TspSimmetricoVi(Adj, k, archi_zero, archi_uno)
    assert(nargin == 4, "Passa tutti gli argomenti\n");
    assert(size(Adj, 1) == size(Adj, 2), "La matrice di adiacenza deve essere quadrata\n");
    assert((isempty(archi_zero) || size(archi_zero, 2) == 2) && (isempty(archi_uno) || size(archi_uno, 2) == 2), "'archi_zero' e 'archi_uno' devono avere esattamente 2 colonne\n");

    vi = [];
    x_vi = Adj - Adj; % x_vi matrice di delle stesse dimensione di Adj piena di zeri
    
    nodi_collegati = [];
    archi_uno_k = [];
    for i = 1:1:size(archi_uno, 1)
        arco = archi_uno(i, :);
        if any(arco == k)
            archi_uno_k = [archi_uno_k; arco;];
            continue; 
        end
        
        x_vi(arco(1), arco(2)) = 1;       
        
        if ~any(nodi_collegati == arco(1))
            nodi_collegati = [nodi_collegati, arco(1)];
        end
            
        if ~any(nodi_collegati == arco(2))
            nodi_collegati = [nodi_collegati, arco(2)];
        end
    end
    
    % se più di 2 archi devono essere per forza collegati k
    if size(archi_uno_k, 1) > 2
        fprintf("Il k-albero non esiste\n");
        x_vi = [];
        vi = [];
    end
    
    A = Adj;        % copio di Adj
    A(A <= 0) = inf;
    A(k, :) = inf;  % isolo in nodo k
    A(:, k) = inf;
    for i = 1:1:size(archi_zero)
        arco = archi_zero(i, :);
        A(arco(1), arco(2)) = inf;
    end
    
    while true
        m = min(A(:));
        if m == inf
            break;
        end
        
        [i, j] = find(A == m, 1);
        A(i, j) = inf;
        
        % controllo se l'inserimento dell'arco causerebbe un ciclo nel
        % grafo
        x_vi_temp = x_vi;
        x_vi_temp(i, j) = 1;
        hasCycle = GraphHasCycle(x_vi_temp);
        if hasCycle == true
            continue;
        end


        x_vi(i, j) = 1;
        
        if ~any(nodi_collegati == i)
            nodi_collegati = [nodi_collegati, i];
        end
            
        if ~any(nodi_collegati == j)
            nodi_collegati = [nodi_collegati, j];
        end
    end
    
    
        
    if length(nodi_collegati) < size(Adj, 1) - 1
        fprintf("Il k-albero non esiste\n");
        x_vi = [];
        vi = [];
        return;
    else


    % connetto il nodo k nel modo più conveniente agli altri nodi    
    A_ = Adj - Adj + inf;
    A_(k, :) = Adj(k, :);
    A_(:, k) = Adj(:, k);
    A_(A_ == 0) = inf;

    for iterazione = 1:1:2
        % do priorità agli archi devono per forza essere collegati a k
        if size(archi_uno_k, 1) > 0
            arco = archi_uno_k(1, :);
            archi_uno_k(1, :) = [];
            
            x_vi(arco(1), arco(2)) = 1;
            continue;
        end
        
        
        m = min(A_(:));
        if m == inf
            fprintf("Il k-albero non esiste\n");
            x_vi = [];
            vi = [];
            return;
        end

        [i, j] = find(A_ == m, 1);
        
        if ~isempty(archi_zero) && ismember([i, j], archi_zero, 'rows')
            fprintf("Il-k albero non esiste\n");
            x_vi = [];
            vi = [];
        end
        
        A_(i, j) = inf;
        x_vi(i, j) = 1;
    end


    % calcolo la vi
    vi = Adj(:)' * x_vi(:);
    
    G = graph(x_vi + x_vi');
    plot(G)
end

