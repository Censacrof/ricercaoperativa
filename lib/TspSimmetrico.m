clc();
clear();

Adj = [ % ATTENZIONE: mettere zeri sotto e lungo la diagonale
    0 30 27 30 49;
    0 0 19 95 63;
    0 0 0 29 28;
    0 0 0 0 61;
    0 0 0 0 0;
];

nodo_partenza_nodo_piu_vicino = 4;
k = 4; 

% fine inputs

% trovo una Vi data dal k-albero di costo minimo
                                     % achi zero
[x_vi, vi] = TspSimmetricoVi(Adj, k, [,], [,])
                                         % archi uno
                                          
% trovo una Vs con l'algoritmo del nodo più vicino
i = nodo_partenza_nodo_piu_vicino;
nodi_visitati = [];
x_vs = Adj - Adj;
A_ = Adj + Adj';
A_(A_ <= 0) = inf;
for iterazione = 1:1:size(A_, 1)
    nodi_visitati = [nodi_visitati, i];    
    
    row = A_(i, :);
    m = min(row(~ismember(1:1:size(A_, 1), nodi_visitati)));
    
    j = [];
    if iterazione == size(A_, 1)
        j = nodo_partenza_nodo_piu_vicino;
    else
        j = find(row == m & ~ismember(1:1:size(A_, 1), nodi_visitati), 1);
    end
    
    assert(~isempty(j), "C'è qualche problema");
    
    if i < j
        x_vs(i, j) = 1;
    else
        x_vs(j, i) = 1;
    end
    i = j;
end
x_vs
vs = Adj(:)' * x_vs(:)
fprintf("ciclo hemiltoniano (Vs):\n");
nodi_visitati