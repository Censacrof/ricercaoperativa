function [hasCycle] = GraphHasCycle(Adj)
    assert(size(Adj, 1) == size(Adj, 2), "La matrice di adiacenza deve esere quadrata\n");

    hasCycle = false;
    
    [n_i n_j] = find(Adj > 0);
    archi = [n_i, n_j];
    
    groups = {};
    for i = 1:1:size(Adj, 1)
        groups{i} = i;
    end   
    
    for iterazione = 1:1:size(archi, 1)
        i = archi(iterazione, 1);
        j = archi(iterazione, 2);
        arco = [i j];
        
        g1 = []; % indice del gruppo 1
        g2 = []; % indice del gruppo 2
        for g = 1:1:size(groups, 2)
            mem = ismember(arco, groups{g});
            
            if all(mem)
               hasCycle = true;
               return;
            end
            
            if mem(1) == 1
                g1 = g;
            end
            
            if mem(2) == 1
                g2 = g;
            end
        end
        
        merged = [groups{g1} groups{g2}];
        groups([g1 g2]) = [];
        groups{end + 1} = merged;
    end
end

